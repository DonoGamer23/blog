import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  posts = [
    { title: 'Mon premier post',
      content: 'Blabla...',
      loveIts: 0,
      created_at: new Date()
    },
    {  title: 'Mon deuxième post',
      content: 'Blublu...',
      loveIts: 2,
      created_at: new Date()
    },
    {  title: 'Mon troisième post',
      content: 'Bloblo...',
      loveIts: -3,
      created_at: new Date()
    }];
}
